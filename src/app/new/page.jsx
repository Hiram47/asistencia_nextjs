"use client"

import { useRouter } from "next/navigation"
import { useForm } from "react-hook-form"





const NewPage = () => {
    const {register,handleSubmit,setValue,watch,formState:{errors}}=useForm()

    const router=useRouter()

    const onSubmit=handleSubmit( async(values)=>{
        console.log(values);

        const res= await fetch('/api/tasks',{
            method:'POST',
            body:JSON.stringify(values),
            headers:{
                'Content-Type':'application/json; charset=utf-8'
            }

        })
        const data= await res.json()
        console.log(data);
        router.push('/')

    })
    
    
    return (
    <div className='h-screen flex justify-center items-center' >
        <form onSubmit={onSubmit} className='bg-slate-800 p-10 w-1/4'>
            <h2 className='text-2xl font-bold py-2'>Registro</h2>
            <input placeholder='Nombre' {...register('name')} className='border border-gray-400 p-2 mb-4 w-full rounded-md text-black' type="text"  />

            <input placeholder='Puesto' {...register('emplyment')} className='border border-gray-400 p-2 mb-4 w-full rounded-md text-black' type="text" />

            <input placeholder='Lugar' {...register('location')} className='border border-gray-400 p-2 mb-4 w-full rounded-md text-black' type="text" />

            <input placeholder='Imagen' {...register('image')} className='border border-gray-400 p-2 mb-4 w-full rounded-md text-black' type="text" />
            {/* <input placeholder='Imagen' {...register('status')} className='border border-gray-400 p-2 mb-4 w-full rounded-md text-black' type="text" /> */}

              <select className='border border-gray-400 p-2 mb-4 w-full rounded-md text-black'  {...register('status')} >
             
            <option value="Activo">Activo</option>
            <option value="Inactivo">Inactivo</option>    

        </select>  

            <button className='bg-sky-600 hover:bg-sky-500 rounded-sm p-2' >Guardar</button>
        </form>
    </div>
  )
}

export default NewPage