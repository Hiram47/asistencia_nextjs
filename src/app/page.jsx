'use client'
import {prisma} from '@/app/libs/prisma'
import { redirect, useRouter } from 'next/navigation';
import { useEffect } from 'react';
async function loadTasks(){
  // primera manera
  const res= await fetch('http://localhost:3000/api/tasks')
  const data = await res.json()

  // segunda manera
  // const data = await prisma.task.findMany({
  //   where:{
  //     status:'Activo'
  //   }
  // })
  console.log(data);
  return data
}





const HomePage = async() => {



  const router=useRouter()
  const tasks= await loadTasks()
  
  //  useEffect(() => {
   
  // //   fetch('http://localhost:3000/api/tasks')
    
  //  }, [])


  
  return (
    <div>
      <h2 className='text-3xl font-bold text-center py-3'>INVITADOS</h2>
      {
      tasks.map(task=>(
        
        <div key={task.id} className='bg-slate-800 my-4 justify-center flex items-center mx-8 ' >

          <img className='scale-50 rounded-full w-96 h-96'  src={task.image}  alt={task.image} />
          <div >

          <h3 className='text-3xl'>{task.name}</h3>
          <h3 className=''>{task.emplyment}</h3>
          <h3 className=''>{task.location}</h3>

          {/* <button onClick={()=>router.push('/task/edit/'+task.id)} className='bg-green-700 py-1 px-2 rounded-md my-2'>Detalles</button> */}

          <button className='bg-green-700 py-1 px-2 rounded-sm my-2' onClick={
            async()=>{

              const res= await fetch('http://localhost:3000/api/tasks/'+task.id,{
                       method:'PUT',
                       body:JSON.stringify({status:'Inactivo'}),
                       headers:{
                        'Content-Type':'application/json; charset=utf-8'
                    }
    
  

                  })

                  router.push('/new')
                  // redirect('/new')
                 // router.refresh()
                  
              return await res.json()


          }} >Asistencia</button>

        </div>
          
        </div>
      ))
      }</div>
  )
}

export default HomePage