import {NextResponse} from 'next/server'
import { prisma } from "@/app/libs/prisma";

export async function GET(request,{params}){
    console.log(params.id);

    const res=await prisma.task.findUnique({
        where:{
            id:+params.id
        }
    })

    
    return NextResponse.json(res)
}

export async function PUT(request,{params}){
    const {status} = await request.json()
    console.log(status);
    const task=await prisma.task.update({
        where:{
            id:+params.id
        },
        //data:data
         data:{status:status}
    })


    return NextResponse.json(task)
}

export async function DELETE(request,{params}){

    try {
        
        const taskRemove = await prisma.task.delete({
            where:{
                id:+ params.id
            }
        })
        return NextResponse.json(taskRemove)
    } catch (error) {
        return NextResponse.json(error.message)
        
    }

}
