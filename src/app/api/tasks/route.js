import { NextResponse } from "next/server";
import { prisma } from "@/app/libs/prisma";


export async function GET(){
    const tasks= await prisma.task.findMany({
        where:{ 
            
                status:'Activo'
                
              
        }
    })
    console.log(tasks);
    ///Consulta RAW
    // const tasks= await prisma.$queryRaw`select * from task where status ='Activo'`
    // console.log(tasks);
    return NextResponse.json(tasks)
}

export async function POST(request){
    try {
        const {name,emplyment,location,image,status}= await request.json()
        console.log(name,emplyment,location,image,status);
       const newtask=await prisma.task.create({
           data:{
               name,
               emplyment,
               location,
               image,
               status
           }
       })
       return NextResponse.json(newtask)
        
    } catch (error) {
        console.log(error);
        
    }
 
}